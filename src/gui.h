﻿#ifndef _GUI_GAME_H_
#define _GUI_GAME_H_

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

typedef struct{
int x;
int y;
int weight;
int heigth;
char text[20];
char text_tip[200];
int num [10];
void * poin [10];
void (* call_b) (SDL_Event event, int [], void * []);
void (* render_b) (SDL_Renderer* gRenderer, int x, int y, int w, int h, char text[], int mouse_x, int mouse_y);
/*void (* render_b_out_mouse) (void);
void (* render_b_on_mouse) (void);
void (* render_b_on_press) (void);
void (* render_b_tip) (void);*/
} button_t;

typedef struct{
int x;
int y;
int weight;
int heigth;
char text[500];
void (* render_t) (SDL_Renderer* gRenderer, int x, int y, int w, int h, char text[]);
} text_field_t;


void gui_render (SDL_Renderer* gRenderer, SDL_Event event, int x_m, int y_m);
void gui_builder ();
void gui_init();
void gui_deinit();

#endif 