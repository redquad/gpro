﻿//Гра "Місто"
//Автор: Тартачний Віталій

#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include "include/yaml.h"
#include "gui.h"
#include "init.h"

int main (int argc, char* argv[])
{
	yaml_emitter_t emitter;
	yaml_parser_t parser;
	yaml_event_t event_yaml;
	//yaml_parser_initialize(&parser);
	yaml_emitter_initialize(&emitter);
	
	run = 1;
	
	SDL_Init(SDL_INIT_EVERYTHING);
	SDL_Window * window;
	SDL_Event event;
	SDL_Renderer* gRenderer = NULL;
    window = SDL_CreateWindow("Віконце", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_OPENGL);
	gRenderer = SDL_CreateRenderer( window, -1, SDL_RENDERER_ACCELERATED );
	int x_mouse = 0;
	int y_mouse = 0;
	
	gui_init();
	
	while (run)
	{
		while (SDL_PollEvent (&event))
		{
			switch (event.type)
			{
				case SDL_QUIT:
					run=0;
					break;
			}
			
			SDL_GetMouseState( &x_mouse, &y_mouse );
			//Clear screen
			SDL_SetRenderDrawColor( gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
			SDL_RenderClear( gRenderer );
			
			gui_builder ();
			gui_render (gRenderer, event, x_mouse, y_mouse);

			//Update screen
			SDL_RenderPresent( gRenderer );
		}
	}
	
	printf("close\n");
	gui_deinit();
	printf("close2\n");
	TTF_Quit();
	printf("close3\n");
	SDL_Quit ();
	return 0;
}