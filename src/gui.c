﻿#include <stdio.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <string.h>
#include "gui.h"
#include "init.h"

void render_exit (SDL_Renderer* gRenderer, int x, int y, int w, int h, char text[], int mouse_x, int mouse_y);
void exit_p (SDL_Event ev, int f[], void * ff[]);
void new_game (SDL_Event ev, int f[], void * ff[]);
int pars_textf (char text[], char (*rtext)[], int fon_w, int fon_h, int w, int h);

int i = 0;
int g_elem_count=0;
button_t * button_array[20];
TTF_Font * font_gui = NULL;
int font_w;
int font_h;

button_t b_exit_game = (button_t){100, 40, 100, 50, "Вийти", "uii", {1}, {&run}, &exit_p, &render_exit};
button_t b_new_game = (button_t){10, 100, 100, 50, "Нова гра", "uii", {}, {}, &new_game, &render_exit};
text_field_t texty = (text_field_t){10, 100, 100, 50, "Нова гра", NULL};


int pars_textf (char text[], char (*rtext)[], int fon_w, int fon_h, int w, int h){
	char * worktext = malloc (2000*sizeof(char));
	char * worktextn = malloc (2000*sizeof(char));
	strcpy(worktextn, text);
	int num_row;
	int num_row_text;
	int div_char;
	int i, j, k;
	if (fon_h>0 && h>0) if(h%fon_h!=0) num_row = h/fon_h+1; else num_row = h/fon_h; else return 1;
	if (fon_w>0 && w>0) div_char = w/fon_w; else return 1;
	worktext = strtok (worktextn, "\n\0");
	do {
		printf("ii\n");
		for (i=0; i<num_row; i++)
		{
			if (i==0){strcat(*rtext, "\t");}
			if (i==num_row-1 || strlen(worktext)<div_char) {
			strcat(*rtext, worktext);
			strcat(*rtext, "\n\0");
			break;}
			else{
				for (j=div_char-1; j>=0; j--){
					if(worktext[j]==' '){
						strncat(*rtext, worktext, j);
						strcat(*rtext, "\n");
						for (k=0; k < strlen ( worktext)-j && strlen ( worktext)>j; k++){
							worktext[k]=worktext[k+j+1];
							if (k == strlen ( worktext)-j-1)
							{
								worktext[k]='\0';
							}
						}
						break;
					}
				}
			}
		}
	} while (worktext = strtok (NULL, "\n\0"));
	printf ("%s\n", *rtext);
	free(worktextn);
	free(worktext);
	return 0;
}

void gui_init ()
{
	TTF_Init();
	font_gui = TTF_OpenFont("FiraMono-Regular.ttf", 20);
	TTF_SizeUTF8 (font_gui, "і", &font_w, &font_h);
	printf("Font size %i %i\n", font_h, font_w);
}

void gui_deinit ()
{
	TTF_CloseFont(font_gui);
}

void add_button (button_t * button)
{	
	button_array[g_elem_count] = button;
	g_elem_count++;
}

void gui_builder ()
{
	g_elem_count=0;
	add_button (&b_exit_game);
	add_button (&b_new_game);
}

void gui_render (SDL_Renderer* gRenderer, SDL_Event ev, int x_m, int y_m)
{
	for (i=0; i<g_elem_count; i++)
	{
		button_array[i]->render_b(gRenderer, button_array[i]->x, button_array[i]->y, button_array[i]->weight, button_array[i]->heigth, button_array[i]->text, x_m, y_m);  //приклад
		if (x_m>button_array[i]->x && x_m<(button_array[i]->x+button_array[i]->weight) && y_m>button_array[i]->y && y_m<(button_array[i]->y+button_array[i]->heigth))
		{
			button_array[i]->call_b(ev, button_array[i]->num, button_array[i]->poin);
		}
	}
}


void render_exit (SDL_Renderer* gRenderer, int x, int y, int w, int h, char text[], int mouse_x, int mouse_y)
{
	int * wt = malloc (sizeof(int));
	int * ht = malloc (sizeof(int));
	SDL_Rect * textRect = malloc (sizeof(SDL_Rect));
	SDL_Rect * fillRect = malloc (sizeof(SDL_Rect));
	SDL_Color * text_color_fg = malloc (sizeof(SDL_Color));
	
	*fillRect = (SDL_Rect){x, y, w, h};
	*text_color_fg = (SDL_Color){ 0, 0, 0 }; 
	
	if (mouse_x>x && mouse_x<(x+w) && mouse_y>y && mouse_y<(y+h)){
				SDL_SetRenderDrawColor( gRenderer, 0xFF, 0x00, 0x00, 0xFF );		
				SDL_RenderFillRect( gRenderer, fillRect );
			} else
			{
				SDL_SetRenderDrawColor( gRenderer, 0x88, 0x00, 0x00, 0x33 );		
				SDL_RenderFillRect( gRenderer, fillRect );
			}
	SDL_Surface* text_surf = NULL;
	text_surf = TTF_RenderUTF8_Blended(font_gui, text, *text_color_fg);
	
	SDL_Texture* text_tur = SDL_CreateTextureFromSurface( gRenderer, text_surf );
	SDL_QueryTexture (text_tur, NULL, NULL, wt, ht); //Отримую значення ширини і висоти з текстури в динамічні змінні wt i ht
	*textRect = (SDL_Rect){x+(w-(*wt))/2, y+(h-(*ht))/2, *wt, *ht};
	
	SDL_RenderCopy (gRenderer, text_tur, NULL, textRect);
	
	SDL_DestroyTexture (text_tur);
	SDL_FreeSurface(text_surf);
	free(wt);
	free(ht);
	free(fillRect);
	free(textRect);
	free(text_color_fg);
}

void exit_p (SDL_Event ev, int f[], void * ff[])
{
	if( ev.type == SDL_MOUSEBUTTONDOWN )
	{
		if( ev.button.button == SDL_BUTTON_LEFT )
		{
			*(int*)ff[0]=0;
		}
	}
}

void new_game (SDL_Event ev, int f[], void * ff[])
{
	if( ev.type == SDL_MOUSEBUTTONDOWN )
	{
		if( ev.button.button == SDL_BUTTON_LEFT )
		{
			printf ("New Game");
		}
	}
}